---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Le tournant démocratique de la Citizen Science
subtitle: Sociologie des transformations d'un programme de science participative
summary: ''
authors:
- admin
- Elise Demeulenaere
- Stéphanie Duvail
- Frédérique Chlous
- Romain Julliard
tags: ['Citizen science', 'démocratisation', 'études des sciences et des techniques']
categories: Article
description: Article
date: '2021'
publishDate: "2022-04-07T00:00:00Z"
featured: false
draft: false
doi: "10.3917/parti.031.0199"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publication_types:
- 2
abstract: |-
  Nous analysons dans cet article les transformations de la citizen science depuis son émergence dans les années 1990 à partir des projets du Cornell Lab of Ornithology (CLO). La citizen science est classiquement définie comme un moyen de produire des connaissances, d’éduquer le public et de démocratiser les sciences. L’analyse diachronique de corpus textuels et des réseaux d’acteurs et actrices montre cependant que l’argument « démocratique » n’apparaît que dans les années 2010, à la différence des deux autres présentes dès les fondements. Cette évolution procède d’un recadrage de l’approche du CLO au prisme de celle que le sociologue des sciences britannique Alan Irwin a conceptualisée Citizen Science (1995).

publication: '*Participations*'
issn: '2268-5863'

#links:
#- name: url
#  url: https://www.cairn.info/revue-participations-2021-3-page-199.htm
---
