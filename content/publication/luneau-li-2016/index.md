---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Li Peiling et Qu Jingdong, La Sociologie Chinoise Avant La Révolution (Traduit
  Du Chinois Par Lucie Modde)
subtitle: ''
summary: ''
authors:
- admin
tags: []
categories: [Note de lecture]
description: Note de lecture
date: '2016-09-01'
lastmod: 2020-12-10T11:13:50+01:00
featured: false
draft: false


# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-10T10:13:50.685481Z'
publication_types:
- 2
abstract: ''
publication: '*Lectures*'
doi: "https://doi.org/10.4000/lectures.21284"


---
