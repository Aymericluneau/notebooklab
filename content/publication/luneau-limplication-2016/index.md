---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: De l'implication du public dans le processus d'expertise à sa prise en compte
  dans les politiques de santé
subtitle: ''
summary: ''
authors:
- admin
- Francis Chateauraynaud
tags: []
categories: [Article]
description: Article
date: '2016-01-01'
lastmod: 2020-12-10T11:13:51+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-10T10:13:50.938561Z'
publication_types:
- 2
abstract: ''
publication: '*Bulletin de veille scientifique*'

links:
- name: pdf
  url: https://bvs.anses.fr/sites/default/files/BVS-mg-028-Luneau_Chateauraynaud.pdf

---
