---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: 'Militants et riverains dans la dynamique des causes environnementales'
subtitle: "Approche sociologique des syndromes d'hypersensibilité chimique"
summary: ""
authors:
- admin
tags: [Santé-environnement, controverses, MCS]
categories: [Thèse]
description: Thèse
date: '2015-03-01'
lastmod: 2020-12-10T11:13:51+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-10T10:13:51.449499Z'
publication_types:
- 7
abstract: ''
publication: 'École des hautes études en sciences sociales'

links:
- name: url
  url : https://halshs.archives-ouvertes.fr/tel-01143362/
---

Thèse pour le doctorat de sociologie de l'École des hautes études en sciences sociales soutenue le 9 mars 2015 à l'École des Mines.

## Résumé

Le 19 octobre 2010, un article du blog Chemical sensitivity network raconte que « certains enfants atteints de MCS [hypersensibilité chimique multiple en français] ont déclaré : “Je souhaiterais avoir un cancer, car au moins mon état serait reconnu” ». Pourquoi, en effet, l’hypersensibilité chimique multiple ne connaît-elle pas la même attention que le cancer ? Cette question pourrait résumer cette thèse dont l’objet est de comprendre les conditions sociales rendant possible l’ouverture d’espaces d’expression concernant les effets environnementaux et sanitaires des substances chimiques. Ce travail de thèse s’appuie sur l’étude de trois problèmes sanitaires et environnementaux. Le premier problème est relatif à l’utilisation du tétrachloroéthylène dans les pressings pour le nettoyage à sec. Le deuxième problème concerne la question des effets sanitaires de la pollution du pourtour de l’étang de Berre, près de Marseille. Enfin, le troisième dossier traite de la controverse sur la réalité du « syndrome d’hypersensibilité chimique multiple » (MCS), qui est une « maladie environnementale » apparue au début des années 1980. L’étude de ces trois problèmes permet d’appréhender la manière dont une dynamique collective se constitue à partir d’« enquêtes » que les riverains, les malades, les victimes, leurs proches, les médecins, les pouvoirs publics entreprennent pour comprendre certaines expériences singulières et résoudre leurs doutes. À travers ces « enquêtes », les acteurs remettent en question certains faits qui semblaient établis, participent à la définition du problème et créent des espaces d’expression sur le lien entre les substances chimiques et la santé. On montrera que ce processus est animé par des dynamiques conflictuelles. Pour cela, il importera d’être attentif aux différentes formes prises par le conflit. Enfin, ces trois problèmes n’existeraient pas sans « profanes » qui transgressent les frontières de l’expertise. Cette thèse constitue l’occasion d’interroger à nouveau cette notion qui est trop souvent réduite à la figure de l’individu désintéressé au sens « utilitariste » et cognitif du terme, et l’individu « passionné », irrationnel voire manipulé.
