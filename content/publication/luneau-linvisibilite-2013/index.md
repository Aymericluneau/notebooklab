---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "L'invisibilité du « syndrome d'hypersensibilité chimique multiple » : les conséquences de l'absence d'un espace de conflit"
subtitle: ''
summary: ''
authors:
- admin
tags:
- axiological conflict
- conflict space
- epistemic conflict
- Multiple chemical sensitivity
- multitude
- MCS
- Controverses
- Santé-environnement
categories: [Article]
description: Article
date: '2013-05-01'
lastmod: 2020-12-10T11:13:51+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-10T10:13:51.191530Z'
publication_types:
- 2
abstract: "The Invisibility of ``Multiple Chemical Sensitivity Syndrom'': the Consequences\
  \ of the lack of Conflict Space. The ``Multiple Chemical Sensitivity syndrome''\
  \ has been at the heart of long conflict, since it was described at the end of the\
  \ seventies. The first goal of article is to show that the connection between pathology\
  \ and pollution is an epistemic and axiological problem. In spite of conflicts,\
  \ the ``Multiple Chemical Sensitivity'' syndrome has failed to stir up much interest\
  \ in France. I will explain this silence by: 1) the lack of a consensual definition\
  \ of the syndrome; 2) lack of an identified conflict space. In order to conclude,\
  \ I would propose to describe environmental health as a multitude."
publication: '*Développement durable et territoires*'
doi: "https://doi.org/10.4000/developpementdurable.9791"
---
