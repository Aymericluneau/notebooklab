---
title: "Involving stakeholders in the risk regulation process: the example of ANSES"
authors:
- admin
- Jean-Michel Fourniau
date: "2019-10-14T00:00:00Z"
doi: "https://doi.org/10.1080/13669877.2019.1687573"

# Schedule page publish date (NOT publication's date).
publishDate: "2019-10-14T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: In *Journal of Risk Research*
publication_short:

abstract: |-
  Since its creation in 2010, the French Agency for Food, Environmental and Occupational Health and Safety (ANSES) has opened up its governance and risk assessment processes to the representatives of professional organisations, NGOs, trade unions and agricultural unions. In 2015, the head of ANSES gave us the opportunity to carry out a one-year inquiry focusing on the design of the agency’s participatory framework and the views of stakeholders about their involvement in work with ANSES. Relying on empirical analysis mixing quantitative and qualitative data, our paper gives an overview of the results of our inquiry.

  Firstly, we will describe how ANSES involves stakeholders. We will show that the agency’s participatory framework articulates a dialogic, consultative and informative procedure. Then, we will explore the reasons why stakeholders participate in ANSES’ committees. While the interviewed stakeholders perceive their membership at the agency as a key measure for strengthening their status in the field of risk governance, they point out the difficulty of seeing whether their participation has influenced ANSES’ strategy. We argue that this ‘lack of clarity’ follows from the segmentation of the three participatory procedures, which undermines the stakeholders’ ability to obtain a broad understanding of how the agency operates. Furthermore, since ANSES is responsible only for risk assessment, stakeholders at the agency’s committees see merely a fraction of a process that operates via other ‘arenas’ such as ministries. As a consequence, stakeholders must have a strong ‘interactional expertise’ to be able to grasp the slightest change in the agency’s strategy, which benefits in the first hand stakeholders that have been deeply involved in the French risk governance.

# Summary. An optional shortened abstract.
summary: |-
  Since its creation in 2010, the French Agency for Food, Environmental and Occupational Health and Safety (ANSES) has opened up its governance and risk assessment processes to the representatives of professional organisations, NGOs, trade unions and agricultural unions.

tags:

categories: Article
description: Article
featured: true

links:
#- name: Custom Link
#  url: http://example.org
#url_pdf: http://eprints.soton.ac.uk/352095/1/Cushen-IMV2013.pdf
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
#image:
  #caption: ""
  #focal_point: ""
  #preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo Academic's Markdown slides feature.
{{% /alert %}}

Supplementary notes can be added here, including [code and math](https://sourcethemes.com/academic/docs/writing-markdown-latex/).
