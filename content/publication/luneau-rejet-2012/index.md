---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Le rejet de l'incinération des ordures ménagères : entre controverses sanitaires
  et conflits politiques"
subtitle: ''
summary: ''
authors:
- admin
tags:
- controverses
- santé-environnement
categories: [Article]
description: Article
date: '2012-09-01'
lastmod: 2020-12-10T11:13:51+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-10T10:13:51.711935Z'
publication_types:
- 2
abstract: ''
publication: '*Environnement, Risques & Santé*'
doi: 10.1684/ers.2012.0565
---
