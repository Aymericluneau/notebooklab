---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Le DICRIM a-t-il transformé les risques chez les Allevardins ?
subtitle: Savoirs et représentations sociales sur le milieu allevardin
summary:
authors:
- admin
tags: []
categories: Rapport de stage
description: Rapport de stage
date: '2008'
lastmod: 2021-03-31T11:13:50+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2008-10-31T10:13:49.474948Z'
publication_types:
- 4
abstract: ''
publication: 'IRMa - Université Pierre Mendès France - EHESS '
issn: ''

links:
- name: url
  url: http://www.irma-grenoble.com/PDF/actualite/articles/Allevard_enquete2_rapport_final.pdf
---
Ce rapport présente les résultats d'une enquête d'un an et demi que j'ai réalisée en tant que stagiaire à l’Institut des risques majeurs (IRMa) de Grenoble. Elle a porté sur les connaissances des habitants de la commune d’Allevard (Isère) concernant les risques majeurs auxquels ils étaient exposés avant et après la mise en place d’une campagne d’information. L’objectif était d’apprécier les effets de la campagne d’information sur les connaissances en matière de risques majeurs. La première phase a été supervisée par Dominique Raynaud (Maître de conférence en sociologie) lorsque j'étais en licence de sociologie à l'Université Pierre Mendès France à Grenoble. La seconde phase a été dirigée par Francis Chateauraynaud (directeur d’études à l’EHESS, Paris) lorsque j'étais en master de sociologie à l'EHESS.
