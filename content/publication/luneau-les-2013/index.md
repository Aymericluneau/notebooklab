---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Les risques toxiques au quotidien
subtitle: Une sociologie politique de l'hypersensibilité
  chimique
summary: ''
authors:
- admin
tags:
- Cartographies
- Logique d'enquête
categories: [Billets]
description: Billets
date: '2013-01-01'
lastmod: 2020-12-10T11:13:50+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-10T10:13:50.405056Z'
publication_types:
- 2
abstract: ''
publication: '*Socio-informatique et argumentation*'

links:
- name: url
  url: https://socioargu.hypotheses.org/4282
---
