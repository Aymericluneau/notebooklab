---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Le Trouble et Son Expérience
subtitle: ''
summary: ''
authors:
- admin
tags: [Controverses sanitaire, santé-environnement, MCS]
categories: [Chapitre de livre]
description: Chapitre de livre
date: '2018-01-01'
lastmod: 2020-12-10T11:13:52+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-10T10:13:52.503680Z'
publication_types:
- 6
abstract: ''
publication: "*(D')Écrire Les Affects. Perspectives et Enjeux Anthropologiques*"
---
