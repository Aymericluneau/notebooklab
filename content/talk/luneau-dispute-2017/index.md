---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: Dispute Sur Les Effets d’une Thérapie \"Neuropsychologique\", Entre Logique
  de Reconnaissance et Logique de “Mieux-Être”
subtitle: ''
summary: ''
authors:
- Aymeric Luneau
tags: []
categories: []
date: '2017-01-01'
lastmod: 2021-02-07T15:35:58+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-02-07T14:35:58.454973Z'
publication_types:
- 3
abstract: ''
publication: ''
url_pdf: https://www.mnhn.fr/fr/visitez/agenda/evenement/tribunes-museum-journee-haut-risque
---
