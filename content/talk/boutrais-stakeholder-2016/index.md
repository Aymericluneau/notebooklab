---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: 'Stakeholder Participation at ANSES: Different Modalities and Evaluation'
subtitle: ''
summary: ''
authors:
- Régine Boutrais
- Aymeric Luneau
tags: []
categories: []
date: '2016-01-01'
lastmod: 2021-02-07T15:35:57+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-02-07T14:35:57.179581Z'
publication_types:
- 3
abstract: ''
publication: ''
url_pdf: https://parisriskgroup.anses.fr/en/minisite/plateforme-shs/paris-risk-group-2016-annual-workshop
---
