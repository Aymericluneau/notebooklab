---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: 'Des Conflits Impossibles : Comprendre L‟issue Des Conflits Au Prisme de Leur
  Existence'
subtitle: ''
summary: ''
authors:
- Aymeric Luneau
tags:
- '"communication"'
categories: []
date: '2013-02-09'
lastmod: 2021-02-07T15:35:58+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-02-07T14:35:57.824351Z'
publication_types:
- 3
abstract: ''
publication: ''
---
