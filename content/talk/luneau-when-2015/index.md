---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: 'When the Regulation of Conflicts Keep Social Movements Going: The Case of
  Environmental Policies'
subtitle: ''
summary: ''
authors:
- Aymeric Luneau
tags: []
categories: []
date: '2015-01-01'
lastmod: 2021-02-07T15:36:07+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-02-07T14:36:06.874905Z'
publication_types:
- 3
abstract: ''
publication: ''
url_pdf: http://programme.esa12thconference.eu/presentation/2290
---
