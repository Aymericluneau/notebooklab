---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: 'Réflexion Sur Le Lien Entre La Modélisation Des Conflits Sociaux et Leurs
  Régulations : Les Cas de Conflits Environnementaux'
subtitle: ''
summary: ''
authors:
- Aymeric Luneau
tags: []
categories: []
date: '2015-01-01'
lastmod: 2021-02-07T15:36:05+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-02-07T14:36:05.211911Z'
publication_types:
- 3
abstract: ''
publication: ''
---
