---
# An instance of the Experience widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: experience

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 40

title: Expériences d'enseignement
subtitle:

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format: Jan 2006

# Experiences.
#   Add/remove as many `experience` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   You can begin a multiline `description` using YAML's `|-`.
experience:
- company: URFIST
  company_url: "https://sygefor.reseau-urfist.fr/#/training/9516/11182?from=true"
  date_end: "2022-05-17"
  date_start: "2022-05-17"
  description: |-
    Atelier d'initiation au logiciel d'analyse de corpus "Prospéro"
  location: En ligne
  title: 'Découverte du logiciel Prospéro : principes et initiation à la socio-informatique'

- company: 'F93'
  company_url: "http://www.f93.fr/"
  date_end: "2022-01-24"
  date_start: "2022-05-31"
  description: |-
    Ateliers d'initiation de collégiens et collégiennes du collège Jean-Vilar de la Courneuve à la sociologie des sciences et des techniques à travers les controverses sur les pesticides. Ateliers réalisés en collaboration avec le toxicologue Olivier Laprévote
  location: La Courneuve
  title: "Effets potentiels"

- company: Sciences Po Saint-Germain-en-Laye
  company_url: ""
  date_end: "2022-01-24"
  date_start: "2022-04-11"
  description: |-
    TD de méthodes consacré à la manipulation de tableurs (libre office calc, Numbers, Excel, Google sheet, etc.) pour réaliser des opérations de statistiques descriptives (indicateurs de tendance centrale, calcul du khi-deux, etc.)
  location: Reims
  title: "Méthodes quantitatives en sciences sociales"

- company: Sciences Po Paris
  company_url: ""
  date_end: "2022-01-17"
  date_start: "2022-01-21"
  description: |-
    Encadrement de deux "séminaires de méthodes" en anglais dans le cadre du cours  [science and society](https://controverses.github.io/science-and-society/) coordonné par Thomas Tari.
  location: Reims
  title: "Sciences et sociétés"

- company: Département de sociologie, Université Paris Ouest-Nanterre
  company_url: "https://dep-socio.parisnanterre.fr/"
  date_end: "2017-08-31"
  date_start: "2016-09-01"
  description:
  location: Nanterre
  title: Attaché temporaire de recherche et d'enseignement

- company: Institut d'urbanisme et d'aménagement régional , Aix-Marseille Université
  company_url: "http://iuar-lieu-amu.fr/"
  date_end: "2012-06-09"
  date_start: "2012-06-05"
  description: |-

    Encadrement sur une semaine d’un groupe de 15 étudiants stagiaires chargés de mener une enquête par questionnaire : Supervision de l’enquête, formation au logiciel de traitement statistique (Tri-deux) et Suivies l’analyse des données.

  location: Aix-en-Provence
  title: Enseignant vacataire, « Stage d’enquête quantitative » (TD)

- company: UFR sciences de l’homme et de la société, Université Paris-Descartes
  company_url:
  date_end: "2015-12-31"
  date_start: "2011-09-01"
  description: |-
    Le TD était consacré aux notions de base en statistiques descriptives : population statistique et les types de variables, représentations graphiques, les indices de tendance centrale, de dispersion et de concentration.
  location: Paris
  title: Enseignant vacataire, « Statistiques descriptives » (TD)

---
