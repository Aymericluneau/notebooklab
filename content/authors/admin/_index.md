---
# Display name
title: Aymeric Luneau

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Chercheur en sociologie

# Organizations/Affiliations to show in About widget
organizations:
- name: médialab
  url: https://medialab.sciencespo.fr/equipe/aymeric-luneau/

# Short bio (displayed in user profile at end of posts)
bio: Je suis sociologue et actuellement postdoctorant sous la direction de Jean-Philippe Cointet au médialab. À travers mes recherches sur les controverses en santé environnement ou l'institutionnalisation des sciences et recherches participatives, je cherche à rendre compte et à comprendre les transformations du processus de production des connaissances. Je m'appuie pour cela sur le croisements des méthodes ethnographiques et de la socio-informatique.

# Interests to show in About widget
interests:
- Sociologie des controverses sanitaires et environnementales
- Citizen Science
- méthodes computationnelles en sciences sociales


# Education to show in About widget
education:
  courses:
  - course: Doctorat de Sociologie
    institution: Ecole des hautes études en sciences sociales
    year: 2015
  - course: Master de recherche en sociologie
    institution: Ecole des hautes études en sciences sociales
    year: 2010
  - course: License de sociologies
    institution: Université Pierre-Mendès France
    year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: mailto:aymeric.luneau@gmail.com
#- icon: https://halshs.archives-ouvertes.fr/public/halshs.png
#  icon_pack: https://halshs.archives-ouvertes.fr/public/halshs.png
#  link: https://twitter.com/GeorgeCushen
#- icon: graduation-cap  # OR `google-scholar`
#  icon_pack: fas  # OR `ai`
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: gitlab
  icon_pack: fab
  link: https://framagit.org/Aymericluneau
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/aymeric-luneau-b507795b/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Email for Contact widget or Gravatar
email: ""

# Organizational groups that you belong to (for People widget)
#   Remove this if you are not using the People widget.
user_groups:
- Principal Investigators

# {{< icon name="download" pack="fas" >}} Download my {{< staticref "media/demo_resume.pdf" "newtab" >}}resumé{{< /staticref >}}.
---
